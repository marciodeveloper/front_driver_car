export class Veiculo {

    constructor(
        public Id_veiculo: number,
        public Placa: string,
        public Data_entrada: string,
        public Data_saida: string
    ) {}
}