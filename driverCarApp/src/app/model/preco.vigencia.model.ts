export class PrecoVigencia {

    constructor(
        public Id_preco_vigencia: number,
        public Data_inicio: string,
        public Data_fim: string,
        public Valor:number,
        public Tipo: number
    ) {}
}