import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { EstacionamentoService } from '../services/estacionamento.service';

import * as moment from 'moment';
import 'moment/locale/pt-br';

const format :string = 'DD-MM-YYYY';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  data = Date.now();
  veiculos: any = [];
  datasEprecos: any = [];
  valorInicial: any;
  valorAdicional: any;

  constructor(private alertController: AlertController, private service: EstacionamentoService) {
  }

  ngOnInit() {
    this.service.getVeiculos().subscribe(result => {
      for (const d of result as any) {
        this.veiculos.push({
          id_veiculo: d.id_veiculo,
          placa: d.placa,
          data_entrada: d.data_entrada,
          data_saida: d.data_saida
        });
      }
    });
    
    this.service.getValoresEstacionamento(moment(Date.now()).format(format)).subscribe(result => {
      for(const p of result as any) {

          if(p.tipo === 1) {
            this.valorInicial = p.valor;
          } else {
            this.valorAdicional = p.valor;
          }
          this.datasEprecos.push({
            Id_preco_vigencia: p.id_preco_vigencia,
            Data_inicio: p.data_inicio,
            Data_fim: p.data_fim,
            Valor: p.valor,
            Tipo: p.tipo
          });
      }
    });
  }

  async presentAlert(veiculo) {
    const alert = await this.alertController.create({
      header: 'Informações do veiculo',
      subHeader: 'Placa : '.concat(veiculo.placa),
      message: this.calculaPermanencia(veiculo),
      buttons: ['OK']
    });

    await alert.present();
  }

  montarMessageAlert(veiculo) {
    if (veiculo.data_saida != null) {
      return '<br>Valor pago:'
    } else {
      return '<br>Valor a pagar:'
    }
  }

  calculaPermanencia(veiculo) {
    if (veiculo.data_saida != null) {
      return 'Total de horas: '.concat(Math.abs(this.calculaHoras(veiculo)).toString());
    }
    return 'Entrada :'.concat(moment(veiculo.data_entrada).format('DD/MM/YYYY HH:mm:ss'));
  }

  calculaHoras(veiculo) {
    const momentEntrada = moment(veiculo.data_entrada);
    let momentSaida: any;
    if(veiculo.data_saida != null) {
      momentSaida = moment(veiculo.data_saida);
    } else {
      momentSaida = moment(Date.now());
    }
    return momentEntrada.diff(momentSaida, 'hours');
  }

}
