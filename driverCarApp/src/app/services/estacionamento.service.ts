import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Veiculo } from '../model/veiculo.model';
import { Observable } from 'rxjs';
import { VeiculoEntrada } from '../model/veiculo.entrada.model';
import { PrecoVigencia } from '../model/preco.vigencia.model';



@Injectable({providedIn: 'root'})
export class EstacionamentoService {

    private urlBase = ' https://localhost:44384/api/estacionamento';

     httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };

    constructor(private http: HttpClient) {}

    getVeiculos(): Observable<Veiculo> {
        return this.http.get<Veiculo>(`${this.urlBase}`);
    }

    entradaDeVeiculo(veiculo: VeiculoEntrada): Observable<Veiculo> {
        return this.http.post<Veiculo>(`${this.urlBase}`, veiculo, this.httpOptions);
    }

    saidaDeVeiculo(veiculo: Veiculo): Observable<Veiculo> {
        return this.http.put<Veiculo>(`${this.urlBase}`, veiculo, this.httpOptions);
    }

    getVeiculoPorPlaca(placa: string): Observable<Veiculo> {
        return this.http.get<Veiculo>(`${this.urlBase}/placa/`.concat(placa));
    }

    getVeiculoPorId(id: any): Observable<Veiculo> {
        return this.http.get<Veiculo>(`${this.urlBase}/`.concat(id));
    }

    getValoresEstacionamento(data: string): Observable<PrecoVigencia> {
        return this.http.get<PrecoVigencia>(`${this.urlBase}/precos/`.concat(data.toString()));
    }

    getValoresEstacionamentoPorTipo(data: string, tipo: any): Observable<PrecoVigencia> {
        return this.http.get<PrecoVigencia>(`${this.urlBase}/precos/`
        .concat(data.toString()).concat('/').concat(tipo));
    }
}


