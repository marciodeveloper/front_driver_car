import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { EstacionamentoService } from '../services/estacionamento.service';

import * as moment from 'moment';
import 'moment/locale/pt-br';

const formatHour :string = 'DD/MM/YYYY HH:mm:ss';
const format :string = 'DD-MM-YYYY';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  data = Date.now();
  placa = '';
  valor = 0;


  constructor(public alertController: AlertController, private service: EstacionamentoService) {}

  onOutCar(){
    this.service.getVeiculoPorPlaca(this.placa)
    .subscribe(result => {
      this.presentAlert(result);
    });
  }

  async presentAlert(veiculo) {
    this.calculaValorPermanencia(veiculo);
    const alert = await this.alertController.create({
      header: 'Saída do veículo',
      subHeader: 'Placa :'.concat(veiculo.placa),
      message: 'Entrada: '
      .concat(moment(veiculo.data_entrada).format(formatHour))
      .concat(' <br> ')
      .concat('Saída: ')
      .concat(moment(Date.now()).format(formatHour))
      .concat(' <br> ')
      .concat('Total de horas: '.concat(this.calculaPermanencia(veiculo)))
      .concat(' <br> ')
      .concat('Valor a pagar: ').concat(this.valor.toString()),
      buttons: [{
        text: 'Confirmar Saída',
        handler: () => {
          this.saidaDeVeiculo(veiculo);
        }}]
    });

    await alert.present();
  }

  calculaValorPermanencia(veiculo) {
    var valorVigente:number;
    var horas:number  = this.calculaHoras(veiculo);
    this.service.getValoresEstacionamentoPorTipo(moment(veiculo.data_entrada).format(format), 1)
    .subscribe(result => {
      for(const v of result as any) {
        if(v.tipo === 1) {
          valorVigente = v.valor;
        }
      }
      this.valor = Math.abs(horas)*valorVigente;
    });
  }

  calculaHoras(veiculo) {
    const momentEntrada = moment(veiculo.data_entrada);
    let momentSaida: any;
    if(veiculo.data_saida != null) {
      momentSaida = moment(veiculo.data_saida);
    } else {
      momentSaida = moment(Date.now());
    }
    return momentEntrada.diff(momentSaida, 'hours');
  }

  calculaPermanencia(veiculo) {
    const momentEntrada = moment(veiculo.data_entrada);
    const momentSaida = moment(Date.now());
    const diferenca = momentEntrada.diff(momentSaida, 'hours');
    return Math.abs(diferenca).toString();
  }


  //TODO servico ainda falhando
  saidaDeVeiculo(veiculo) {
    veiculo.data_saida = moment(Date.now());
    this.service.saidaDeVeiculo(veiculo);
  }

}
