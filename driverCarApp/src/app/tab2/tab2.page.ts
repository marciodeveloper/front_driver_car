import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { EstacionamentoService } from '../services/estacionamento.service';

import * as moment from 'moment';
import 'moment/locale/pt-br';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  data = Date.now();
  placa = '';

  constructor(public alertController: AlertController, private service: EstacionamentoService) {}

  onInputCar(){
    const veiculo = {
      placa: this.placa,
      data_entrada: moment(Date.now())
    }
    this.presentAlert(veiculo);
  }

  async presentAlert(veiculo) {
    const alert = await this.alertController.create({
      header: 'Entrada do Veículo',
      subHeader: 'Placa :'.concat(veiculo.placa),
      message: 'Entrada :'.concat(veiculo.data_entrada),
      buttons: [ {
        text: 'OK',
        handler: () => {
          this.entradaDeVeiculo(veiculo);
        }}]
    });

    await alert.present();
  }

  entradaDeVeiculo(veiculo) {
    this.service.entradaDeVeiculo(veiculo).subscribe(result => {
      console.log(result);
    });
  }

}
